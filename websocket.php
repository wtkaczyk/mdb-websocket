<?php  

// before do anything read https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers.

class ws_server{
  var $master;
  var $sockets = array(); //create an array of socket objects 
  var $users   = array(); //create an array of users objects to handle discussions with users
  var $debug   = false;
  
  function ascii_banner() //just for old-skool fun...
  {
    $banner ="               _    ____             _        _   \n";
    $banner.="__      _____ | |__/ ___|  ___   ___| | _____| |_ \n ";
    $banner.="\ \ /\ / / _ \|  _ \__  \ / _ \ / __| |/ / _ \ __| \n";
    $banner.=" \ V  V /  __/| |_)|__)  | (_) | (__|   <  __/ |_ \n";
    $banner.="  \_/\_/ \____|_.__/____/ \___/ \___|_|\_\___|\__|\n";
    return $banner;
                                                 
  }
  
  function __construct($address,$port){

      $chatHandler = new ChatHandler();
      $null = NULL;

      error_reporting(E_ALL);
      set_time_limit(0);
      ob_implicit_flush();

      $this->master=socket_create(AF_INET, SOCK_STREAM, SOL_TCP)     or die("socket_create() failed");
      socket_set_option($this->master, SOL_SOCKET, SO_REUSEADDR, 1)  or die("socket_option() failed");
      socket_bind($this->master, $address, $port)                    or die("socket_bind() failed");
      socket_listen($this->master,44444)                             or die("socket_listen() failed");
      $this->sockets[] = $this->master;

      $chatHandler->say($this->ascii_banner() );
      $chatHandler->say("PHP WebSocket Server running....");
      $chatHandler->say("Server Started : ".date('Y-m-d H:i:s'));
      $chatHandler->say("Listening on   : ".$address." port ".$port);
      $chatHandler->say("Master socket  : ".$this->master."\n");
      $chatHandler->say(".... awaiting connections ...");

      $clientSocketArray = array($this->master);

      /*
        creating while loop to keep function running 
        new connection going to newSocketArray
      */
      while (true) {
          $newSocketArray = $clientSocketArray;

          /*
            socket_select accepts arrays of sockets and waits for them to change status.
          */
          socket_select($newSocketArray, $null, $null, null);
          // $chatHandler->say("listening...\n");
          
          // check if incoming socket is known socket. ($this->master is incoming socket)
          if (in_array($this->master, $newSocketArray)) {
              // accept socket and return socket resource that dont need to be accepted anymore
              $newSocket = socket_accept($this->master);
              // add new socket resource to known sockets.
              $clientSocketArray[] = $newSocket;

              $chatHandler->connect($newSocket);

              $user = $chatHandler->getuserbysocket($newSocket);

              // recive $buffer from socket
              $bytes = @socket_recv($newSocket,$buffer,2048,0);

              // congratulations!! you did a handshake!
              $chatHandler->doHandshake($user,$buffer,$address,$port);
              
              // remove current socket from newSocketArray
              $newSocketIndex = array_search($this->master, $newSocketArray);
              unset($newSocketArray[$newSocketIndex]);
          }
          
          // here goes sockets that are known - this mean that those are events
          foreach ($newSocketArray as $newSocketArrayResource) {
              // get $buffer and checking bytes - if bytes are empty -> disconnect user - if not -> process it (send event to the next step)
              $bytes = @socket_recv($newSocketArrayResource,$buffer,2048,0);
              if($bytes==0){ 

                $chatHandler->disconnect($newSocketArrayResource); 
                $newSocketIndex = array_search($newSocketArrayResource, $clientSocketArray);
                unset($clientSocketArray[$newSocketIndex]);     

              } else {
                $chatHandler->process($user,$chatHandler->frame_decode($buffer) ); 
              }
          }
      }
      // $chatHandler->say('$this->master');
      socket_close($this->master);
    }
  }
