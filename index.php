<?php
    require __DIR__ . '/vendor/autoload.php';
define('HOST_NAME',"127.0.0.1"); 
define('PORT',"44444");

require_once("class.chathandler.php");
require_once("websocket.php");

$master = new ws_server(HOST_NAME,PORT);
